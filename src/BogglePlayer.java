import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/*
 * Author:  Connor Roth, croth2016@my.fit.edu
 * Course:  CSE 2010, Section 03, Fall 2017
 * Project:
 */

/**
 * @author connor
 * @author calvin
 *
 */
public class BogglePlayer {
    //fields of the boggle player class
    static CompressedTrie wordTree;
    static int row = 0;
    static int col = 0;

    /**
     * The constructor for Boggle Player.
     * @param wordFile A file containing a list of valid English words.
     * @throws FileNotFoundException 
     */
    public BogglePlayer(String wordFile) throws FileNotFoundException {
        wordTree = new CompressedTrie();
        wordTree.createTrie(new File(wordFile));
    }
    /**
     * This method takes a board as input from the EvalBogglePlayer and performs DFS on all of the characters in the board.
     * It the reorders all of the words from longest to shortest to maximize points.
     * 
     * @param board
     * @return Array of 20 or less Words
     */
    public Word[] getWords(char[][] board) {
        row = board.length;
        col = board[0].length;                                                      //get the amount of rows and columns
        Search search = new Search(wordTree, board);
        ArrayList<Word> validWords = search.startLetters();                         //An arraylist to contain all of the valid words
        Collections.sort(validWords);                                               //sort this list in order of the longest words to the shortest words
        ArrayList<Word> noDuplicates = new ArrayList<>(0);                          //this arraylist is used when getting rid of duplicate words
        Set<String> strings = new HashSet<>(0);
        //because two Word objects might have the same String word, they could have entire differnt paths, so there
        //needs to be a way to eliminate words with the same path
        for(Word w : validWords) {                                                  //for each word in Valid words
            if(strings.add(w.getWord())) {                                          //add the String of each Word to a set
                noDuplicates.add(w);                                                //if the string can be added, add the Word to the noDups list
            }                                                                       //if it cant add, that means the Word String is already added and this one is a duplicate
        }
        validWords = null;                                                          //null this out for memory reasons
        Word[] output;
        Collections.sort(noDuplicates);                                             //sort these in order of the longest word
        if (noDuplicates.size() > 20) {                                                    //if the arraylist of Words is larger than 20
            for(int z = noDuplicates.size() -1 ; z >19 ; z--) {                            //remove elements until the list is of length 20
                noDuplicates.remove(z);
            }
            output = new Word[noDuplicates.size()];
        } else {
            output = new Word[20];                                                  //otherwise fill the list as much as it can be
        }

        for(int z = 0; z < noDuplicates.size(); z++) {
            output[z] = (noDuplicates.get(z));                                      //add the Words to the output array
        }
        return output;
        

    }

    public static void main(String[] args) throws FileNotFoundException {

    }


}
