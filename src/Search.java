import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Search {
    //fields 
    CompressedTrie dict;
    char[][] board;
    int row ;
    int col ;
    boolean[][] visited ;
    ArrayList<Word> words = new ArrayList<>(0);
    /**
     * Constructor for the search class. 
     * @param wordTree The trie of all of the words.
     * @param board The board of characters
     */
    public Search(CompressedTrie wordTree, char[][] board) {
        dict = wordTree;
        this.board = board;
        row = board.length;
        col = board[0].length;
        visited = new boolean[row][col];
    }
    /**
     * This method will cycle through each char on the board and use it as a first letter. It will the call getWords on 
     * that letter and that method get every word that results from the neighboring chars
     * @return the ArrayList of words that were found
     */
    public ArrayList<Word> startLetters() {
        for (int i = 0; i < row; i ++) {
            for (int j = 0; j < col; j++) {
                visited[i][j] = true;                                               //mark this 'cell' as visited
                Character c = board[i][j];                                          //get the char from this cell
                Word w = new Word(c.toString());                                    //form a new word from this char
                ArrayList<Location> wordPath = new ArrayList<>(0);                  //create a new path to keep track of Locations
                wordPath.add(new Location (i,j));                                   //add location
                getWord(w,i,j,wordPath);                                            //call getWord
                visited[i][j] = false;                                              //set to non visited so it can be included in future words 
            }
        }
        return words;                                                               //return the list of found words
    }
    /**
     * This method recursively checks each neighbor in a depth first search. As it builds words it checks the progress in the trie.
     * If it reaches a non word path it returns to the previous until it finds another word.
     * @param word
     * @param i
     * @param j
     * @param path
     */
    public void getWord(Word word, int i, int j, ArrayList<Location> path) {
        visited[i][j] = true;                                                       //mark this location on the board as visited
        int value = dict.findWord(word);                                            //gets either 1,-1,0 depending if the word is in the trie
        if(value == 1) {                                                            //if the word is a complete word in the trie
            ArrayList<Location> list = new ArrayList<>(path.size());                //make a new list of locations so the pointers dont get messed up
            list.addAll(path);
            word.setPath(list);                                                     //set the path of the word
            words.add(word);                                                        //add the word to the list of all found words
        }else if (value == -1){                                                     //if the word is not found in the trie and isnt still being constructed
            return;                                                                 //back out of this path and try the other surrounding children of the 'parent'
        }
        Character c;
        Word w;
        //not the bottom row
        if (i < row-1) {                                                            //make sure the index wont give a null pointer exception
            if (!visited[i+1][j]) {                                                 //if you havent visted the spot below
                c = board[i+1][j];                  
                w = new Word(word.getWord() + c.toString());                        //concat a new word with the character of the cell below
                path.add(new Location(i+1,j));                                      //add location to the path
                getWord(w,i+1,j,path);                                              //go in to the next level 
                path.remove(path.size()-1);                                         //remove the last path entry
                visited[i+1][j] = false;                                            //set visited to false so other words with this letter can be made, no longer a part of the last word
            }
            //bottom right diagonal
            if (j < col-1 && !visited[i+1][j+1]) {                                  //make sure the index wont give a null pointer exception, and the next spot hasnt been visited yet
                c = board[i+1][j+1];                                                //grab the next char
                w = new Word(word.getWord() + c.toString());                        //concat a new word with the character of the cell below and to the right
                path.add(new Location(i+1,j+1));                                    //add location to the path
                getWord(w,i+1,j+1,path);                                            //go into the next level
                path.remove(path.size()-1);                                         //remove the last path entry
                visited[i+1][j+1] = false;                                          //set visited to false so other words with this letter can be made, no longer a part of the last word
                //right
                if (!visited[i][j+1]) {
                    c = board[i][j+1]; 
                    w = new Word(word.getWord() + c.toString());                    //concat a new word with the character of the cell to the right
                    path.add(new Location(i, j+1));
                    getWord(w,i,j+1,path);
                    path.remove(path.size()-1);
                    visited[i][j+1] = false;                                        //set visited to false so other words with this letter can be made, no longer a part of the last word
                }   
            }
            //not the left most one, check the left
            if (j > 0) {     
                //left
                if (!visited[i][j-1]) {
                    c = board[i][j-1];
                    w = new Word(word.getWord() + c.toString());                     //concat a new word with the character of the cell to the left
                    path.add(new Location(i,j-1));
                    getWord(w,i,j-1,path);
                    path.remove(path.size()-1);
                    visited[i][j-1] = false;                                        //set visited to false so other words with this letter can be made, no longer a part of the last word
                }
                //bottom left diagonal
                if(!visited[i+1][j-1]) {
                    c = board[i+1][j-1];
                    w = new Word(word.getWord() + c.toString());                    //concat a new word with the character of the cell below and left
                    path.add(new Location(i+1,j-1));
                    getWord(w,i+1,j-1,path);
                    path.remove(path.size()-1);
                    visited[i+1][j-1] = false;                                     //set visited to false so other words with this letter can be made, no longer a part of the last word
                }
            }
        }
        //bottom row accounted for
        if (i <= row-1) {
            //not the left most one, check the left
            if (j > 0) {
                if (!visited[i][j-1]) {
                    c = board[i][j-1];
                    w = new Word(word.getWord() + c.toString());                 //concat a new word with the character of the left
                    path.add(new Location(i,j-1));
                    getWord(w,i,j-1,path);
                    path.remove(path.size()-1);
                    visited[i][j-1] = false;                                     //set visited to false so other words with this letter can be made, no longer a part of the last word
                }

            }
        }
        //not in the top row
        if (i > 0) {
            //check above
            if (!visited[i-1][j]) {
                c = board[i-1][j];
                w = new Word(word.getWord() + c.toString());                    //concat a new word with the character of the cell above
                path.add(new Location( i-1, j));
                getWord(w,i-1,j,path);
                path.remove(path.size()-1);
                visited[i-1][j] = false;                                        //set visited to false so other words with this letter can be made, no longer a part of the last word
            }
            //check the top right diagonal
            if(j < col-1 && !visited[i-1][j+1]) {
                c = board[i-1][j+1];
                w = new Word(word.getWord() + c.toString());                    //concat a new word with the character of the cell above and to the right
                path.add(new Location(i-1,j+1));
                getWord(w,i-1,j+1,path);
                path.remove(path.size()-1);
                visited[i-1][j+1] = false;                                      //set visited to false so other words with this letter can be made, no longer a part of the last word
            }
            if (j > 0)
                //check the top left diagonal
                if (!visited[i-1][j-1]) {
                    c = board[i-1][j-1];
                    w = new Word(word.getWord() + c.toString());                //concat a new word with the character of the cell above and to the left
                    path.add(new Location(i-1,j-1));
                    getWord(w,i-1,j-1,path);
                    path.remove(path.size()-1);
                    visited[i-1][j-1] = false;                                  //set visited to false so other words with this letter can be made, no longer a part of the last word
                }
        }

    }
}
