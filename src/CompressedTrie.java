
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;


/*
 * Author:  Connor Roth, croth2016@my.fit.edu
 * Course:  CSE 2010, Section 03, Fall 2017
 * Project: 
 */
/**
 * This Compressed Trie is a linked structure. It is not compressed like in the textbook. It acts like a regular Trie
 * but instead of storing Characters, it stores a smaller datatype of Byte. It has an "encoded alphabet" that converts 'A' to 1,
 * 'B' to 2 etc... Thats how the information from the board interacts with the trie.
 * @author Connor
 *
 */
public class CompressedTrie {
    CompressedNode root;                                                            //Root Node value will be 0
    Map<Character,Integer> alphabet;                                                //A maping of letters as explained above
  /**
   * Constructor for the CompressedTrie
   */
    public CompressedTrie() {   
        root = new CompressedNode();                                                //defualt node element is 0;
        alphabet = new HashMap<>(0);
        Character startChar = 'A';
        //in a loop, map each letter to a number
        for(int i = 1; i < 27; i++,startChar++){
            alphabet.put(startChar, i);
        }
    }
    /**
     * this got messed up in the compression part
     * @param word
     */
    public void addWord(String word) {
        char[] letters = word.toUpperCase().toCharArray();
        CompressedNode current = this.root;
        int i = 0;
        int l = letters.length;
        while ( i < l ) {
            //check if the current node has children.
            //if it doesnt there just add the node
            if (!current.hasChildren()) {
                CompressedNode child = new CompressedNode(alphabet.get(letters[i]).byteValue());      //the node is a new compressed node whos value is gotten with a key                                                                  
                current.addChild(child);                                                              //of the current letter, and is passed in as the byte
                current = child;
                if (i == l-1) {
                    child.stop = true;
                }
            } else {
                //if there is children,  get the child if it exists
                CompressedNode child;
                boolean added = false;
                child = current.getChild(alphabet.get(letters[i]).byteValue());
                if (child != null) {
                    if (child.element == alphabet.get(letters[i]).byteValue()) {
                        current = child;                                            //set the child to current and mark it as added
                        added = true;   
                        if (i == l-1) {
                            child.stop = true;                                      //if the letter is the last letter in the word mark it as a stop value
                        }
                    }
                }
                //if it wasnt added, create a new node
                if(!added) {
                    child = new CompressedNode(alphabet.get(letters[i]).byteValue());
                    current.addChild(child);
                    current = child;
                    added = true;
                    if (i== l-1) {
                        child.stop = true;
                    }
                }
            }
            i++;                                                                    //go to the next letter in the word
        }
    }

    /**
     * This method adds words that ar 3 letters or more to the trie
     * @param dictionary
     * @throws FileNotFoundException
     */
    public void createTrie(File dictionary) throws FileNotFoundException {
        Scanner scan = new Scanner(dictionary);
        while (scan.hasNextLine()) {
            String w = scan.nextLine(); 
            if (w.length() >2)
                this.addWord(w);

        }
    }
    /**
     * This method attempts to find the word given in the Tire
     * @param word
     * @return 1 if found a full word, 0 if its in the tree but not complete, -1 if not in tree
     */
    public int findWord(Word s) {
        String word = s.getWord().toUpperCase();                                    //Make the word Capital String
        char[] c = word.toCharArray();                                              //Make the string a char array
        Integer[] compressedWord = new Integer[c.length];                           //make an integer array of the same length
        for(int i = 0; i <c.length; i ++ ) {                                        //get the numbers that correspond to the letters
            compressedWord[i] = alphabet.get(c[i]);
        }
        CompressedNode current = this.root;
        for (int i = 0; i < c.length; i++) {
            CompressedNode child = current.getChild(compressedWord[i].byteValue());       //see if there is a child of the node with that byte
            if (child != null) {                                                            //if there is, see if the last 'letter' is the stop val
                if(i == c.length - 1) {
                    if(child.stop){
                        return 1;                                                           //return 1 if a word is found
                    }else {
                        return 0;                                                           //return 0 if the word is still being formed
                    }

                } 
                current = child;
            }
        }
        return -1;                                                                      //return -1 if there are no children that have the next letters byte val (ie, the word is gibberish after the current letter)
    }   

    public static void main(String[] args) {
 
    }
    

}
