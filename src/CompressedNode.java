import java.util.ArrayList;


public class CompressedNode{
        byte element;                           //element data is a byte not a char, saves space
        ArrayList<CompressedNode> children;
        boolean stop = false;                   //tells you if this letter is the end of a word
        /**
         * Default Constructor for a compressed node.
         */
        public CompressedNode() {
            element = 0;                        //default value is 0
            children = new ArrayList<>(0);
        }
        public CompressedNode(byte e) {
            this();                             //same is super
            element = e;                        //element is passed in
        }
        /**
         * This constructor essentially clones a node that is passed in, eliminates pointer mistakes
         * @param n
         */
        public CompressedNode(CompressedNode n) {
            this.element = n.element;
            this.children = n.children;
        }
        /**
         * This method adds a child to the current node.
         * @param child
         */
        public void addChild(CompressedNode child) {
            this.children.add(child);
        }
        /**
         * @return True if current node has children
         */
        public boolean hasChildren() {
            return this.children.size()>0;
        }
        /**
         * This method finds a child node if it is given a target element.
         * @param target
         * @return
         */
        public CompressedNode getChild(byte target) {
            CompressedNode child = null;
            for (int i = 0; i < this.children.size(); i ++) {           //unfortunately this is linear, couldnt makek binary search work.
                child = this.children.get(i);
                if (child.element == target) {
                    return child;
                }
            }
            return null;
        }
    }